import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview'

const restart = () => {
  location.reload();
}

export function showWinnerModal(fighter) {
  // call showModal function

  const { name: title } = fighter;
  const bodyElement = createFighterImage(fighter);

  showModal({ title, bodyElement, onClose: restart });  // on close - redirect to start;
}
