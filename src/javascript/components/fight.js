import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    let pressed = new Set();
    let firstFighterCriticalHitLastTime = 0;
    let secondFighterCriticalHitLastTime = 0;

    document.addEventListener('keydown', function(event) {
      pressed.add(event.code);
      console.log(pressed)

      // Critical Hit

      if (controls.PlayerOneCriticalHitCombination.every(value => pressed.has(value))) {

        if (Date.now() - firstFighterCriticalHitLastTime > 10000) {
          let healthBar = document.getElementById('right-fighter-indicator');
          let damage = 2 * firstFighter.attack;
          let new_width = computeHealthBarWidth(firstFighter, secondFighter, damage, healthBar);
          healthBar.style.width = `${new_width}px`;

          secondFighter.health -= 2 * firstFighter.attack;
          firstFighterCriticalHitLastTime = Date.now();
        }

      } else if (controls.PlayerTwoCriticalHitCombination.every(value => pressed.has(value))) {

        if (Date.now() - secondFighterCriticalHitLastTime > 10000) {
          let healthBar = document.getElementById('left-fighter-indicator');
          let damage = 2 * secondFighter.attack;
          let new_width = computeHealthBarWidth(secondFighter, firstFighter, damage, healthBar);;
          healthBar.style.width = `${new_width}px`;

          firstFighter.health -= 2 * secondFighter.attack;
          secondFighterCriticalHitLastTime = Date.now();
        }

      // Single Attack

      } else if (pressed.has(controls.PlayerOneAttack) && !(pressed.has(controls.PlayerTwoBlock) || pressed.has(controls.PlayerOneBlock))) {

        let healthBar = document.getElementById('right-fighter-indicator');
        let damage = getDamage(firstFighter, secondFighter);
        let new_width = computeHealthBarWidth(firstFighter, secondFighter, damage, healthBar);
        healthBar.style.width = `${new_width}px`;

        secondFighter.health -= getDamage(firstFighter, secondFighter);
        console.log(secondFighter.health);
        console.log(getDamage(firstFighter, secondFighter));

      } else if (pressed.has(controls.PlayerTwoAttack) && !(pressed.has(controls.PlayerOneBlock) || pressed.has(controls.PlayerTwoBlock))) {

        let healthBar = document.getElementById('left-fighter-indicator');
        let damage = getDamage(secondFighter, firstFighter);
        let new_width = computeHealthBarWidth(secondFighter, firstFighter, damage, healthBar);;
        healthBar.style.width = `${new_width}px`;

        firstFighter.health -= getDamage(secondFighter, firstFighter);

      }

      // check winner
      if (firstFighter.health <= 0) {
        resolve(secondFighter);
      } else if (secondFighter.health <= 0) {
        resolve(firstFighter);
      }
    });

    document.addEventListener('keyup', function(event) {
      pressed.delete(event.code);
    });

  });
}

export function computeHealthBarWidth(attacker, defender, damage, healthBar) {
  let new_width = (defender.health - damage) * healthBar.clientWidth / defender.health;
  return new_width > 0 ? new_width : 0;
}

export function getDamage(attacker, defender) {
  // return damage

  let damage = getHitPower(attacker) - getBlockPower(defender);
  return (damage > 0 ? damage : 0);
}

export function getHitPower(fighter) {
  // return hit power

  let criticalHitChance = Math.random() + 1;
  let power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return block power

  let dodgeChance = Math.random() + 1;
  let power = fighter.defense * dodgeChance;
  return power;
}
